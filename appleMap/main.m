//
//  main.m
//  appleMap
//
//  Created by Click Labs130 on 10/13/15.
//  Copyright (c) 2015 Click Labs130. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HomeAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HomeAppDelegate class]));
    }
}
