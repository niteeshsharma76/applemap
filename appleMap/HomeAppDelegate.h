//
//  HomeAppDelegate.h
//  appleMap
//
//  Created by Click Labs130 on 10/13/15.
//  Copyright (c) 2015 Click Labs130. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface HomeAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
